all:
	gcc -o main.o main.c -lGLESv2 -lGLEW -lglfw -I../cutils -L../glutils -L../cutils -lcutils -I../glutils -lglutils -g
run:
	LD_LIBRARY_PATH=../cutils:../glutils ./main.o
clean:
	rm main.o
