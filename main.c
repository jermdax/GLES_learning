#include <GL/glew.h>
#include <GLES2/gl2.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "cutils.h"
#include "glutils.h"

static GLfloat tri[] = {
	-0.5f, -0.5f, 0.f,
	-0.5f, 0.5f, 0.f,
	0.5f, -0.5f, 0.f,

	-0.5f, 0.5f, 0.f,
	0.5f, 0.5f, 0.f,
	0.5f, -0.5f, 0.f,
};

void print_shader_errors(GLuint shader)
{
	GLint compiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if(!compiled)
	{
		fprintf(stderr, "Compilation of fragment shader failed\n");
		GLint infoLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if(infoLen > 1)
		{
			char* infoLog = malloc(sizeof(char) * infoLen);
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			printf("Error compiling shader:\n%s\n", infoLog);
			free(infoLog);
		 }
	}
}

int main(int argc, char* argv[])
{
	if(!glfwInit())
	{
		fprintf(stderr, "Failed to init glfw\n");
		return 1;
	}

	GLFWwindow* wind =  glfwCreateWindow(640, 480, "BeepBoop", NULL, NULL);


	glfwMakeContextCurrent(wind);
	glewExperimental = GL_TRUE;
	glewInit();
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	const char *vert = ftostr("test.vert");
	const char *frag = ftostr("test.frag");
	struct model* mod = load_obj("test.obj");
	assert(vert != NULL);
	assert(frag != NULL);
	assert(mod != NULL);

	struct tri_arr t_arr = model_to_tri_arr(mod);

	GLuint vbo = 0;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	printf("allocing %d floats in vbo\n", t_arr.nfloats);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * t_arr.nfloats, t_arr.tris, GL_STATIC_DRAW);

	const unsigned int floats_per_point = 3;
	unsigned int nverts = t_arr.nfloats/floats_per_point;
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL); //TODO WTF does 3 mean here

	// vert
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vert, NULL);
	glCompileShader(vs);
	print_shader_errors(vs);
	// frag
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &frag, NULL);
	glCompileShader(fs);
	print_shader_errors(fs);

	GLuint shader_prog = glCreateProgram();
	glAttachShader(shader_prog, fs);
	glAttachShader(shader_prog, vs);
	glBindAttribLocation(shader_prog, 0, "vPosition");
	glLinkProgram(shader_prog);

	while(!glfwWindowShouldClose(wind))
	{
		if(GLFW_PRESS == glfwGetKey(wind, GLFW_KEY_ESCAPE))
			glfwSetWindowShouldClose(wind, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(0.75, 0.75, 0.75, 1);
		glUseProgram(shader_prog);
		glDrawArrays(GL_TRIANGLES, 0, nverts);
		glfwPollEvents();
		glfwSwapBuffers(wind);
	}

	glfwTerminate();
	free((void*)vert);
	free((void*)frag);
	while(mod != NULL)
	{
		struct model* temp = mod;
		mod = temp->next;
		free((void*)temp); 
	}
	free((void*)t_arr.tris);
	return 0;
}
